CREATE OR REPLACE TRIGGER illTriggerAdd
before insert on LesMaladies
for each row
begin
	UPDATE LesAnimaux SET nb_maladies = nb_maladies + 1
	WHERE nomA = :new.nomA;
end;