
import java.sql.*;

public class squelette_appli {

    static final String CONN_URL = "jdbc:oracle:thin:@im2ag-oracle.e.ujf-grenoble.fr:1521:im2ag";
    //static final String CONN_URL = "jdbc:mysql://localhost:3306/bt_tp2";

    static final String USER = "medardt";
    static final String PASSWD = "cOdesses85";
    /*static final String USER = "root";
     static final String PASSWD = "cOdesses8520!";*/

    static final String CHANGE_CAGE_FUNCTION = "c";
    static final String ADD_ANIMAL = "a";
    static final String MOVE_ANIMAL = "m";
    static final String COMMIT = "com";
    static final String DISPLAY = "d";
    static final String QUIT = "q";

    static Boolean quit = false;

    static Connection conn;

    public static void main(String args[]) {

        try {

            // Enregistrement du driver Oracle
            System.out.print("Loading Oracle driver... ");
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
            System.out.println("loaded");
                        // MySQL test
                        /*try {
             Class.forName("com.mysql.jdbc.Driver");
             } catch (ClassNotFoundException e) {
             System.out.println("Where is your MySQL JDBC Driver?");
             return;
             }*/

            // Etablissement de la connection
            System.out.print("Connecting to the database... ");
            conn = DriverManager.getConnection(CONN_URL, USER, PASSWD);
            System.out.println("connected");

            // Desactivation de l'autocommit
            conn.setAutoCommit(false);
            System.out.println("Autocommit disabled");

            String menu = "Menu : \n"
                    + CHANGE_CAGE_FUNCTION + " : change cage function\n"
                    + ADD_ANIMAL + " : add animal\n"
                    + MOVE_ANIMAL + " : move animal\n"
                    + COMMIT + " : commit\n"
                    + DISPLAY + " : display\n"
                    + QUIT + " : quit";

            String input;
            while (!quit) {
                System.out.println("");
                System.out.println(menu);
                input = LectureClavier.lireChaine();
                System.out.println("");

                switch (input) {
                    case CHANGE_CAGE_FUNCTION:
                        Transactions.changeCageFunction(conn);
                        break;
                    case ADD_ANIMAL:
                        Transactions.addAnimal(conn);
                        break;
                    case MOVE_ANIMAL:
                        Transactions.moveAnimal(conn);
                        break;
                    case COMMIT:
                        conn.commit();
                        break;
                    case DISPLAY:
                        Transactions.display(conn);
                        break;
                    case QUIT:
                        quit = true;
                        break;
                }
                System.out.println("");
            }

            // code metier de la fonctionnalite

            // Liberation des ressources et fermeture de la connexion...
            conn.close();

            System.out.println("bye.");

            // traitement d'exception
        } catch (SQLException e) {
            System.err.println("failed");
            System.out.println("Affichage de la pile d'erreur");
            e.printStackTrace(System.err);
            System.out.println("Affichage du message d'erreur");
            System.out.println(e.getMessage());
            System.out.println("Affichage du code d'erreur");
            System.out.println(e.getErrorCode());

        }
    }

}
