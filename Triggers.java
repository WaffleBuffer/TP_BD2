
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author medardt
 */
public abstract class Triggers {

    public static void illTriggerAdd (Connection conn) {

        PreparedStatement stmt = null;
        ResultSet res = null;

        try {
            
            String query = "CREATE OR REPLACE TRIGGER illTriggerAdd "
                    + "before insert on LesMaladies "
                    + "for each row "
                    + "begin "
                    + "UPDATE LesAnimaux SET nb_maladies = nb_maladies + 1 "
                    + "WHERE nomA = :new.nomA; "
                    + "end";
            
            stmt = conn.prepareStatement(query);
            
            stmt.executeQuery();
            
            conn.commit();

        } catch (SQLException ex) {
            Logger.getLogger(Transactions.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (res != null) {
                    res.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(Transactions.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
