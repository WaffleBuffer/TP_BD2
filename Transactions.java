

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author medardt
 */
public abstract class Transactions {

        public static void displayBD(Connection conn, String tableName) {

                PreparedStatement stmt = null;
                ResultSet res = null;
                try {

                        System.out.println(tableName + " :");
                        stmt = conn.prepareStatement("SELECT * FROM " + tableName);
                        //stmt.setString(1, tableName);
                        res = stmt.executeQuery();

                        for (int i = 1; i < res.getMetaData().getColumnCount(); ++i) {
                                System.out.print(res.getMetaData().getColumnName(i) + " ");
                        }

                        System.out.println("");
                        while (res.next()) {

                                for (int i = 1; i < res.getMetaData().getColumnCount(); ++i) {
                                        System.out.print(res.getString(i) + " ");
                                }
                                System.out.println("");
                        }

                } catch (SQLException ex) {
                        Logger.getLogger(Transactions.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                        try {
                                if (stmt != null) {
                                        stmt.close();
                                }
                                if (res != null) {
                                        res.close();
                                }
                        } catch (SQLException ex) {
                                Logger.getLogger(Transactions.class.getName()).log(Level.SEVERE, null, ex);
                        }
                }
        }
        
        public static void display(Connection conn) {
            System.out.println("Table ?");
            String table = LectureClavier.lireChaine();
            
            displayBD(conn, table);
        }

        public static void changeCageFunction(Connection conn) {

                System.out.println("Fonction ?");
                String function = LectureClavier.lireChaine();
                int noCage = LectureClavier.lireEntier("NoCage ?");

                PreparedStatement stmt = null;

                try {
                        stmt = conn.prepareStatement("UPDATE LesCages SET fonction = ? "
                                + "WHERE noCage = ?");

                        stmt.setString(1, function);
                        stmt.setInt(2, noCage);

                        stmt.execute();
                } catch (SQLException ex) {
                        Logger.getLogger(Transactions.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                        if (stmt != null) {
                                try {
                                        stmt.close();
                                } catch (SQLException ex) {
                                        Logger.getLogger(Transactions.class.getName()).log(Level.SEVERE, null, ex);
                                }
                        }
                }
        }

        private static String readSex() {
                Boolean checked = false;
                String input = "";
                while (!checked) {
                        System.out.println("Sex ? (male, femelle, hermaphrodite)");
                        input = LectureClavier.lireChaine();

                        if (input.equals("male") || input.equals("femelle") || input.equals("hermaphrodite")) {
                                checked = true;
                                break;
                        } else {
                                System.err.println("Invalid sex");
                        }
                }

                return input;
        }

        private static int readBirthYear() {
                Boolean checked = false;
                int input = 0;
                while (!checked) {
                        input = LectureClavier.lireEntier("Year ? > 1900");

                        if (input > 1900) {
                                checked = true;
                                break;
                        } else {
                                System.err.println("invalid birth year");
                        }
                }

                return input;
        }

        private static String chooseFunction(Connection conn) {
                Boolean checked = false;
                String input = "";
                PreparedStatement stmt = null;
                ResultSet res = null;
                while (!checked) {

                        System.out.println("Fonction cage?");
                        input = LectureClavier.lireChaine();

                        try {
                                stmt = conn.prepareStatement("SELECT * FROM LesCages WHERE fonction = ?");

                                stmt.setString(1, input);

                                res = stmt.executeQuery();

                                if (res.next()) {
                                        checked = true;
                                } else {
                                        System.err.println("Invalid cage function");
                                }

                        } catch (SQLException ex) {
                                Logger.getLogger(Transactions.class.getName()).log(Level.SEVERE, null, ex);
                                break;
                        }
                }

                try {
                        if (stmt != null) {
                                stmt.close();
                        }
                        if (res != null) {
                                res.close();
                        }
                } catch (SQLException ex) {
                        Logger.getLogger(Transactions.class.getName()).log(Level.SEVERE, null, ex);
                }

                return input;
        }

        /**
         * Make the user choose a cage number with a provided function
         *
         * @param conn The connection to the data base
         * @param fonction The function of the cage
         * @return The id of the cage
         */
        private static int chooseCageByAnimalType(Connection conn, String fonction) {
                Boolean checked = false;
                int input = 0;
                PreparedStatement stmt = null;
                ResultSet res = null;
                while (!checked) {

                        try {
                                stmt = conn.prepareStatement("SELECT * FROM LesCages WHERE fonction = ?");

                                stmt.setString(1, fonction);

                                res = stmt.executeQuery();

                                for (int i = 1; i < res.getMetaData().getColumnCount(); ++i) {
                                        System.out.print(res.getMetaData().getColumnName(i) + " ");
                                }

                                System.out.println("");
                                while (res.next()) {

                                        for (int i = 1; i < res.getMetaData().getColumnCount(); ++i) {
                                                System.out.print(res.getString(i) + " ");
                                        }
                                        System.out.println("");
                                }
                        } catch (SQLException ex) {
                                Logger.getLogger(Transactions.class.getName()).log(Level.SEVERE, null, ex);
                                break;
                        }

                        input = LectureClavier.lireEntier("What cage number ?");

                        try {               
                            
                                res = stmt.executeQuery();
                                while (res.next()) {
                                        if (res.getInt(1) == input) {
                                                checked = true;
                                                break;
                                        } else {
                                                System.err.println("Invalid cage number");
                                        }
                                }
                        } catch (SQLException ex) {
                                Logger.getLogger(Transactions.class.getName()).log(Level.SEVERE, null, ex);
                                break;
                        }
                }

                try {
                        if (stmt != null) {
                                stmt.close();
                        }
                        if (res != null) {
                                res.close();
                        }
                } catch (SQLException ex) {
                        Logger.getLogger(Transactions.class.getName()).log(Level.SEVERE, null, ex);
                }

                return input;
        }

        public static void addAnimal(Connection conn) {

                System.out.println("Nom ?");
                String name = LectureClavier.lireChaine();

                String sex = readSex();

                System.out.println("Type animal ?");
                String typeAn = LectureClavier.lireChaine();

                String foncCage = chooseFunction(conn);

                System.out.println("Pays ?");
                String land = LectureClavier.lireChaine();

                int year = readBirthYear();

                int noCage = chooseCageByAnimalType(conn, foncCage);

                int noMal = LectureClavier.lireEntier("Nb maladie ?");
                PreparedStatement stmt = null;

                try {
                        stmt = conn.prepareStatement("INSERT INTO LesAnimaux "
                                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)");

                        stmt.setString(1, name);
                        stmt.setString(2, sex);
                        stmt.setString(3, typeAn);
                        stmt.setString(4, foncCage);
                        stmt.setString(5, land);
                        stmt.setInt(6, year);
                        stmt.setInt(7, noCage);
                        stmt.setInt(8, noMal);

                        stmt.execute();
                } catch (SQLException ex) {
                        Logger.getLogger(Transactions.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                        if (stmt != null) {
                                try {
                                        stmt.close();
                                } catch (SQLException ex) {
                                        Logger.getLogger(Transactions.class.getName()).log(Level.SEVERE, null, ex);
                                }
                        }
                }
        }

        private static String chooseAnimale(Connection conn) {
                Boolean checked = false;
                String input = "";
                PreparedStatement stmt = null;
                ResultSet res = null;
                while (!checked) {

                        try {
                                stmt = conn.prepareStatement("SELECT * FROM lesAnimaux");

                                res = stmt.executeQuery();

                                for (int i = 1; i < res.getMetaData().getColumnCount(); ++i) {
                                        System.out.print(res.getMetaData().getColumnName(i) + " ");
                                }

                                System.out.println("");
                                while (res.next()) {

                                        for (int i = 1; i < res.getMetaData().getColumnCount(); ++i) {
                                                System.out.print(res.getString(i) + " ");
                                        }
                                        System.out.println("");
                                }

                                System.out.println("Annimal name?");
                                input = LectureClavier.lireChaine();

                                res = stmt.executeQuery();
                                //res.beforeFirst();
                                while (res.next()) {
                                        if (res.getString(1).equals(input)) {
                                                checked = true;
                                                break;
                                        }
                                }

                                if (!checked) {
                                        System.err.println("Invalid animal name");
                                }

                        } catch (SQLException ex) {
                                Logger.getLogger(Transactions.class.getName()).log(Level.SEVERE, null, ex);
                                break;
                        }
                }

                try {
                        if (stmt != null) {
                                stmt.close();
                        }
                        if (res != null) {
                                res.close();
                        }
                } catch (SQLException ex) {
                        Logger.getLogger(Transactions.class.getName()).log(Level.SEVERE, null, ex);
                }

                return input;
        }

        private static String getAnimalCageFunction(Connection conn, String animalName) {
                String input = "";
                PreparedStatement stmt = null;
                ResultSet res = null;

                try {
                        stmt = conn.prepareStatement("SELECT * FROM lesAnimaux WHERE nomA = ?");

                        stmt.setString(1, animalName);

                        res = stmt.executeQuery();

                        if (res.next()) {
                                input = res.getString(4);
                        }

                } catch (SQLException ex) {
                        Logger.getLogger(Transactions.class.getName()).log(Level.SEVERE, null, ex);
                }

                try {
                        if (stmt != null) {
                                stmt.close();
                        }
                        if (res != null) {
                                res.close();
                        }
                } catch (SQLException ex) {
                        Logger.getLogger(Transactions.class.getName()).log(Level.SEVERE, null, ex);
                }

                return input;
        }

        public static void moveAnimal(Connection conn) {

                String animalName = chooseAnimale(conn);
                String cageFunction = getAnimalCageFunction(conn, animalName);

                int numCage = chooseCageByAnimalType(conn, cageFunction);

                PreparedStatement stmt = null;
                try {
                        stmt = conn.prepareStatement("UPDATE LesAnimaux SET noCage = ? WHERE nomA = ?");

                        stmt.setInt(1, numCage);
                        stmt.setString(2, animalName);

                        stmt.execute();

                } catch (SQLException ex) {
                        Logger.getLogger(Transactions.class.getName()).log(Level.SEVERE, null, ex);
                }

                try {
                        if (stmt != null) {
                                stmt.close();
                        }
                } catch (SQLException ex) {
                        Logger.getLogger(Transactions.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
}
